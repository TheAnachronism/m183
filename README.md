# M183 - Applikationssicherheit implementieren 
### Tobias Gröli

Dieses Repository enthält alle gelösten Praxisarbeiten, welche innerhalb des Moduls 184 im GIBZ erarbeitet wurden.

## 02 XSS Logger
Ein Key logger ist ein kleines script oder programm, welches alle Tastenanschläge aufzeichnet und z.B. an einen Server schickt. Diese können dann ausgewertet werden und so kann vielleicht ein getipptes Passwort ausgelesen werden.

Teil dieser Aufgabe war nur einen solchen Key Logger zu schreiben. Diesen dann in eine Webseite einzufügen war dabei nicht Thema.

## 03 Multifactor Authentication
Bei Multifactor Authentication spricht man von Prinzip, dass man mehrere Dinge angeben muss, wenn man sich wo anmelden will. Also nicht nur Email und Passwort sondern auch noch weiter informationen.

Innerhalb dieser Praxisarbeit wurde das System von TOTP (Timebased-One-Time-Passwort) implementiert. Das heisst, um sich anzumelden, muss man nicht nur die Email und das Passwort angeben, sondern auch ein zufällig generierten Code, welcher durch einen zuvor gescannten Qr-Code generiert wird. Dadurch kann man sich nur anmelden, wenn man die Login-Daten und diesen Code hat.

## 04 Click Hijacking
Bei Click-Hijacking geht es darum, dass man eine Zielwebseite hinter seiner eigenen Webseite versteckt und sobald der User auf der eigenen Webseite einen preparierten Button drückt, geht dieses Clickevent aber an die Zielwebseite. Damit kann man z.B. dafür sorgen, dass der User etwas auslöst, was er nicht wollte und dies nicht einmal bemerkt.

## 05 Sichere Passwörter
Wenn man sich irgendwo registriert, wird mittlerweile überall gesagt, dass man bestimmte voraussetzungen für ein Passwort haben soll. Eine bestimmte Zeichenlänge oder Zahlen und Sonderzeichen müssen vorhanden sein. Diese Praxisarbeit hat sich damit beschäftigt indem man sicherstellen soll, dass der User ein genügend kompliziertes Passwort eingibt.

## 06 Single-Sign-On
Die Idee von Single-Sign-On ist, dass man sich mit einem bestimmten Account (z.B. Google oder Facebook) bei anderen Webseiten anmelden kann. Innerhalb dieser Praxisarbeite wurde dies mit Google gelöst und bestimmte Userdaten werden dann einfach in einer temporären Datenbank gespeichert.

## 07 Http-Digest
Http-Digest ist eine authentifizierungsmethode, welche vom Http-Protokoll selber unterstützt wird. Dabei wird ein .htpasswd.txt mitgeben, welches die verschlüsselte Version des Passwortes und den Username enthält. Bei Anfrage, wird dieses Überprüft und der User muss die Anmelde Daten in einem kleinen Pop-up, welches vom Browser geöffnet wird, eingeben.