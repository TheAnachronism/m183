using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SingleSignOn.Models;
using SingleSignOn.Models.DataTransferObjects;
using SingleSignOn.Services;

namespace SingleSignOn.Controllers
{
    public class UserController : Controller
    {
        /**
         * The user manager is a handy tool for various operations regarding User objects.
         * It might be used to create and/or retrieve users.
         */
        private readonly UserManager<User> _userManager;

        private readonly SignInManager<User> _signInManager;

        /**
         * Constructor of the UserController.
         * The userManager dependency is provided by dependency injection.
         */
        public UserController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        /**
         * This action is used for verification of the provided id token.
         * If the token is valid, an existing user with the validated user data is retrieved from the user store (using the _userManager).
         * If no user with the validated user data exists, a new user object is created and added to the user user store.
         *
         * Finally, the (existing|created) user gets authenticated and a http status 200 (Ok) is returned.
         *
         * If the validation of the id token with the identity providers backend fails, a http status 400 (BadRequest) is returned.
         */
        [HttpPost]
        public async Task<IActionResult> VerifyIdToken([FromBody] IdTokenVerificationRequest verificationRequest)
        {
            var validPayload = await GoogleJsonWebSignature.ValidateAsync(verificationRequest.IdToken);
            if (validPayload == null)
                return BadRequest();

            User currentUser = null;

            if (!_userManager.Users.Any(x => x.Email == validPayload.Email))
            {
                currentUser = await CreateUser(validPayload.Email, validPayload.GivenName, validPayload.FamilyName);
            }
            else
                currentUser = await _userManager.FindByEmailAsync(validPayload.Email);

            await AuthenticateUser(currentUser);

            await _signInManager.RefreshSignInAsync(currentUser);

            return Ok();
        }

        /**
         * Creates a new user object with the given data and adds it to the user store.
         * If the creation and persistence of the user object succeeded, the user object is returned. In case of failure, null is returned.
         */
        private async Task<User> CreateUser(string email, string firstName, string lastName)
        {
            var user = new User
            {
                UserName = email,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
            };
            var result = await _userManager.CreateAsync(user);

            return result.Succeeded ? user : null;
        }

        /**
         * Authenticates the given user object using the cooke authentication scheme.
         * The cookie (and therefore the session) will last for 1 day.
         */
        private async Task AuthenticateUser(User user)
        {
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                ExpiresUtc = DateTimeOffset.Now.AddDays(1),
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(UserService.CreateClaimsIdentity(user)),
                authProperties
            );
        }
    }
}