/**
 * This message gets called when the authentication process with google succeeded.
 *
 * @param googleUser
 */
function onSuccess(googleUser) {
    console.log(`Logged in as: ${googleUser.getBasicProfile().getName()}`);

    const idToken = googleUser.getAuthResponse().id_token;
    console.log(idToken);

    const requestBody = JSON.stringify({
        "idToken": idToken
    });

    // TODO: Send an http POST request to the verification endpoint to validate the id token.
    // In case you'll receive an 415 http status: Consider to set the Content-Type request header to 'application/json'.
    // This page might be helpful: https://developers.google.com/identity/sign-in/web/backend-auth
    const verificationEndpoint = "/User/VerifyIdToken";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", verificationEndpoint);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function() {
      console.log(`Signed in as: ${xhr.responseText}`);
    };
    xhr.send(requestBody);
    

    // When the validation was successful, the user is fully authenticated.

    // TODO: In case of successful validation/authentication: redirect to the profile endpoint.
    const redirectionEndpoint = "/Profile";
    window.location = redirectionEndpoint;
}