namespace SingleSignOn.Models.DataTransferObjects
{
    public class IdTokenVerificationRequest
    {
        public string IdToken { get; set; }
    }
}