# Tobias & Dzenis

# Password Anforderungen
Wir haben uns einfach für die Standardvorgaben für ein Password entschieden.
Gross- sowie Kleinschreibung muss beides enthalten sein, sowohl als auch Nummern.
Sonderzeichen könnte man auch noch verwenden / erzwingen, aber das wäre dann zuviel für den Normalnutzer, da viele keine Passwortmanager und immer wieder das gleiche Passwort verwenden.
Mit der Anforderung, dass das Password zehn Zeichen lang sein muss, wird dies mit alleine der Gross-Kleinschreibung und den Nummern schon genügen.

# Password speichern
Dadurch, dass wir das Password mit BCrypt und einem Salt hashen, können wir diesen Hash sicher auf der Datenbank speichern. Der Salt wird dabei natürlich auch gespeichert, da sonst in Zukunft nicht geprüft werden kann, ob der Nutzer das richtige Password eingegeben hat.

# Passwort Sicherheit
Durch das Verwenden eines Salts, können Accounts, welche das gleiche Passwort haben, nicht gehackt werden, da sich dort die Salts und somit auch der ganze Hash unterscheidet.